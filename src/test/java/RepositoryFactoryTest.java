import com.company.app.enums.RepositoryFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RepositoryFactoryTest {

    @Test
    void shouldReturnRoleRepository() {
        Assertions.assertNotNull(RepositoryFactory.INSTANCE.getRoleRepository());
    }

    @Test
    void shouldReturnUserRepository() {
        Assertions.assertNotNull(RepositoryFactory.INSTANCE.getUserRepository());
    }

    @Test
    void shouldReturnTablewareRepository() {
        Assertions.assertNotNull(RepositoryFactory.INSTANCE.getTablewareRepository());
    }

    @Test
    void shouldReturnHouseholdGoodRepository() {
        Assertions.assertNotNull(RepositoryFactory.INSTANCE.getHouseholdGoodRepository());
    }
}
