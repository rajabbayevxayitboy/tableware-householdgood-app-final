import com.company.app.domains.HouseholdGood;
import com.company.app.enums.RepositoryFactory;
import com.company.app.repository.HouseholdGoodRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class HouseholdGoodRepositoryTest {

    @Test
    void shouldNotNull() {
        HouseholdGoodRepository repository = RepositoryFactory.INSTANCE.getHouseholdGoodRepository();
        Optional<List<HouseholdGood>> householdGoodsOpt = repository.getAll();
        Assertions.assertTrue(householdGoodsOpt.isPresent());
    }
}
