import com.company.app.domains.auth.Role;
import com.company.app.enums.RepositoryFactory;
import com.company.app.repository.auth.RoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class RoleRepositoryTest {

    @Test
    void shouldNotNull() {
        RoleRepository repository = RepositoryFactory.INSTANCE.getRoleRepository();
        Optional<List<Role>> rolesOpt = repository.getAll();
        Assertions.assertTrue(rolesOpt.isPresent());
    }

    @Test
    void shouldFindAll() {
        RoleRepository repository = RepositoryFactory.INSTANCE.getRoleRepository();

        List<Role> expectedList = List.of(
                new Role(1L, "Administrator", "ADMIN"),
                new Role(2L, "User", "USER")
        );

        Assertions.assertArrayEquals(expectedList.toArray(), repository.getAll().get().toArray());
    }
}
