import com.company.app.domains.auth.User;
import com.company.app.enums.RepositoryFactory;
import com.company.app.repository.auth.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class UserRepositoryTest {

    @Test
    void shouldNotNull() {
        UserRepository repository = RepositoryFactory.INSTANCE.getUserRepository();
        Optional<List<User>> usersOpt = repository.getAll();
        Assertions.assertTrue(usersOpt.isPresent());
    }

    @Test
    void shouldDefaultAdmin() {
        UserRepository repository = RepositoryFactory.INSTANCE.getUserRepository();
        User admin = new User(
                1L, "admin", "123", "John", "Johnson", "21", "San-Fransisco"
        );
        Assertions.assertNotNull(repository.getAll().get().stream().filter(user -> user.equals(admin)));
    }
}
