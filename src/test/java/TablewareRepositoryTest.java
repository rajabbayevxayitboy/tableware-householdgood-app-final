import com.company.app.domains.Tableware;
import com.company.app.enums.RepositoryFactory;
import com.company.app.repository.TablewareRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class TablewareRepositoryTest {

    @Test
    void shouldNotNull() {
        TablewareRepository repository = RepositoryFactory.INSTANCE.getTablewareRepository();
        Optional<List<Tableware>> tablewaresOpt = repository.getAll();
        Assertions.assertTrue(tablewaresOpt.isPresent());
    }
}
