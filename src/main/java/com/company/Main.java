package com.company;

import com.company.client.AppUI;
import com.company.client.constants.Constants;
import com.company.client.helper.ConsoleHelper;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        ConsoleHelper.println(Constants.APP_NAME + " version: " + Constants.VERSION + "  " + new Date());
        ConsoleHelper.println("Developer name: " + Constants.DEV_NAME);
        ConsoleHelper.println("Developer email: " + Constants.DEV_EMAIL);
        AppUI.run();
    }
}