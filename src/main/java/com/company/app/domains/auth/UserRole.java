package com.company.app.domains.auth;

import com.company.app.domains.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserRole extends AbstractEntity {
    private Long userId;
    private Long roleId;
}
