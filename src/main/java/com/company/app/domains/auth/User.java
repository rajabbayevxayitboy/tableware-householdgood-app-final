package com.company.app.domains.auth;

import com.company.app.domains.AbstractEntity;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class User extends AbstractEntity {
    private Long id;
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private String age;
    private String address;
}
