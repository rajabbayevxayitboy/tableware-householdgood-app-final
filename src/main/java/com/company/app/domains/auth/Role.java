package com.company.app.domains.auth;

import com.company.app.domains.AbstractEntity;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Role extends AbstractEntity {
    private Long id;
    private String name;
    private String code;
}
