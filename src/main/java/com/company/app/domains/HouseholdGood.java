package com.company.app.domains;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class HouseholdGood extends AbstractEntity {
    private Long id;
    private Double price;
    private Integer count;
    private String manufacturer;
    private String name;
    private String color;
    private String creationDate;
}
