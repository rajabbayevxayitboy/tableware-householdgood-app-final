package com.company.app.enums;

import com.company.app.service.HouseholdGoodService;
import com.company.app.service.TablewareService;
import com.company.app.service.auth.AuthService;
import com.company.app.service.auth.RoleService;
import com.company.app.service.auth.UserService;

public enum ServiceFactory {
    INSTANCE;

    public TablewareService getTablewareService() {
        return new TablewareService(RepositoryFactory.INSTANCE.getTablewareRepository());
    }

    public HouseholdGoodService getHouseholdGoodService() {
        return new HouseholdGoodService(RepositoryFactory.INSTANCE.getHouseholdGoodRepository());
    }

    public UserService getUserService() {
        return new UserService(RepositoryFactory.INSTANCE.getUserRepository());
    }

    public RoleService getRoleService() {
        return new RoleService(RepositoryFactory.INSTANCE.getRoleRepository(), RepositoryFactory.INSTANCE.getUserRoleRepository(), ServiceFactory.INSTANCE.getUserService());
    }

    public AuthService getAuthService() {
        return new AuthService(getUserService(), getRoleService());
    }
}
