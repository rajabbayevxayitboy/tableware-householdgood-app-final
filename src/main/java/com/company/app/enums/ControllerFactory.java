package com.company.app.enums;

import com.company.app.controller.HouseholdGoodController;
import com.company.app.controller.TablewareController;
import com.company.app.controller.auth.AuthController;
import com.company.app.controller.auth.RoleController;
import com.company.app.controller.auth.UserController;

public enum ControllerFactory {
    INSTANCE;

    public TablewareController getTablewareController() {
        return new TablewareController(ServiceFactory.INSTANCE.getTablewareService());
    }

    public HouseholdGoodController getHouseholdGoodController() {
        return new HouseholdGoodController(ServiceFactory.INSTANCE.getHouseholdGoodService());
    }

    public UserController getUserController() {
        return new UserController(ServiceFactory.INSTANCE.getUserService());
    }

    public RoleController getRoleController() {
        return new RoleController(ServiceFactory.INSTANCE.getRoleService());
    }

    public AuthController getAuthController() {
        return new AuthController(ServiceFactory.INSTANCE.getAuthService());
    }
}
