package com.company.app.enums;

import com.company.app.repository.HouseholdGoodRepository;
import com.company.app.repository.TablewareRepository;
import com.company.app.repository.auth.RoleRepository;
import com.company.app.repository.auth.UserRepository;
import com.company.app.repository.auth.UserRoleRepository;

public enum RepositoryFactory {
    INSTANCE;

    public TablewareRepository getTablewareRepository() {
        return new TablewareRepository();
    }

    public HouseholdGoodRepository getHouseholdGoodRepository() {
        return new HouseholdGoodRepository();
    }

    public UserRepository getUserRepository() {
        return new UserRepository();
    }

    public RoleRepository getRoleRepository() {
        return new RoleRepository();
    }

    public UserRoleRepository getUserRoleRepository() {
        return new UserRoleRepository();
    }
}
