package com.company.app.controller;

import com.company.app.controller.base.BaseActionController;
import com.company.app.controller.base.BaseController;
import com.company.app.controller.base.BaseReadController;
import com.company.app.domains.Tableware;
import com.company.app.dto.ResponseEntity;
import com.company.app.enums.ResponseStatus;
import com.company.app.service.TablewareService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class TablewareController implements BaseReadController<Tableware>, BaseActionController<Tableware>, BaseController {

    private TablewareService service;

    @Override
    public ResponseEntity<List<Tableware>> getAll() {
        return new ResponseEntity<>(service.getAll(), ResponseStatus.SUCCESS);
    }

    @Override
    public ResponseEntity<Tableware> get(Map<String, String> params) {
        return new ResponseEntity<>(service.get(params), ResponseStatus.SUCCESS);
    }

    @Override
    public ResponseEntity<Long> create(Map<String, String> params) {
        return new ResponseEntity<>(service.create(params), ResponseStatus.SUCCESS);
    }

    @Override
    public ResponseEntity<Boolean> delete(Map<String, String> params) {
        return new ResponseEntity<>(service.delete(params), ResponseStatus.SUCCESS);
    }
}
