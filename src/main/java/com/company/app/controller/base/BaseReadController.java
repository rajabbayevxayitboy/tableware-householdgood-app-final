package com.company.app.controller.base;

import com.company.app.domains.AbstractEntity;
import com.company.app.dto.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface BaseReadController<T extends AbstractEntity> {

    ResponseEntity<List<T>> getAll();

    ResponseEntity<T> get(Map<String, String> params);
}
