package com.company.app.controller.base;

import com.company.app.dto.ResponseEntity;

import java.util.Map;

public interface BaseAuthController<T> {
    ResponseEntity<T> login(Map<String, String> params);

    ResponseEntity<T> register(Map<String, String> params);
}
