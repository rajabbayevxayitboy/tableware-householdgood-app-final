package com.company.app.controller.base;

import com.company.app.domains.AbstractEntity;
import com.company.app.dto.ResponseEntity;

import java.util.Map;

public interface BaseActionController<T extends AbstractEntity> {
    ResponseEntity<Long> create(Map<String, String> params);

    ResponseEntity<Boolean> delete(Map<String, String> params);
}
