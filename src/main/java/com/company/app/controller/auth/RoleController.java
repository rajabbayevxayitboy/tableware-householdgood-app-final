package com.company.app.controller.auth;

import com.company.app.controller.base.BaseController;
import com.company.app.controller.base.BaseReadController;
import com.company.app.domains.auth.Role;
import com.company.app.dto.ResponseEntity;
import com.company.app.enums.ResponseStatus;
import com.company.app.service.auth.RoleService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class RoleController implements BaseReadController<Role>, BaseController {

    private RoleService service;

    @Override
    public ResponseEntity<List<Role>> getAll() {
        return new ResponseEntity<>(service.getAll(), ResponseStatus.SUCCESS);
    }

    @Override
    public ResponseEntity<Role> get(Map<String, String> params) {
        return new ResponseEntity<>(service.get(params), ResponseStatus.SUCCESS);
    }

    public ResponseEntity<Boolean> attachRole(Map<String, String> params) {
        return new ResponseEntity<>(service.attachRole(params), ResponseStatus.SUCCESS);
    }
}
