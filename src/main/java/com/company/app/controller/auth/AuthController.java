package com.company.app.controller.auth;

import com.company.app.controller.base.BaseAuthController;
import com.company.app.controller.base.BaseController;
import com.company.app.dto.ResponseEntity;
import com.company.app.enums.ResponseStatus;
import com.company.app.service.auth.AuthService;
import lombok.AllArgsConstructor;

import java.util.Map;

@AllArgsConstructor
public class AuthController implements BaseController, BaseAuthController<String> {

    private AuthService service;

    @Override
    public ResponseEntity<String> login(Map<String, String> params) {
        return new ResponseEntity<>(service.login(params), ResponseStatus.SUCCESS);
    }

    @Override
    public ResponseEntity<String> register(Map<String, String> params) {
        return new ResponseEntity<>(service.register(params), ResponseStatus.SUCCESS);
    }
}
