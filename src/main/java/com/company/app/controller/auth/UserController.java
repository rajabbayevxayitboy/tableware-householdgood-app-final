package com.company.app.controller.auth;

import com.company.app.controller.base.BaseController;
import com.company.app.controller.base.BaseReadController;
import com.company.app.domains.auth.User;
import com.company.app.dto.ResponseEntity;
import com.company.app.enums.ResponseStatus;
import com.company.app.service.auth.UserService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class UserController implements BaseReadController<User>, BaseController {

    private UserService service;

    @Override
    public ResponseEntity<List<User>> getAll() {
        return new ResponseEntity<>(service.getAll(), ResponseStatus.SUCCESS);
    }

    @Override
    public ResponseEntity<User> get(Map<String, String> params) {
        return new ResponseEntity<>(service.get(params), ResponseStatus.SUCCESS);
    }
}
