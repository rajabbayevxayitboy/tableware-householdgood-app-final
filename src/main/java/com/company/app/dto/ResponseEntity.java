package com.company.app.dto;

import com.company.app.enums.ResponseStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseEntity<T> {
    private T data;
    private ResponseStatus status;
}
