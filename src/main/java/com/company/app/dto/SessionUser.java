package com.company.app.dto;

import com.company.app.domains.auth.Role;
import com.company.app.domains.auth.User;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SessionUser {
    private static SessionUser instance;
    private User sessionUser;
    private Role role;

    public static SessionUser getInstance() {
        if (instance == null) {
            instance = new SessionUser();
        }
        return instance;
    }
}
