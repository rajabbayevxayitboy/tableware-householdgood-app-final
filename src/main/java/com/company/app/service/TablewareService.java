package com.company.app.service;

import com.company.app.domains.Tableware;
import com.company.app.exception.AppRuntimeException;
import com.company.app.repository.TablewareRepository;
import com.company.app.service.base.BaseActionService;
import com.company.app.service.base.BaseReadService;
import lombok.AllArgsConstructor;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@AllArgsConstructor
public class TablewareService implements BaseActionService<Tableware>, BaseReadService<Tableware> {

    private TablewareRepository repository;

    @Override
    public Long create(Map<String, String> params) {
        String price = params.get("price");
        if (price.isBlank())
            throw new AppRuntimeException("Price can not be null!");

        String count = params.get("count");
        if (count.isBlank())
            throw new AppRuntimeException("Count can not be null!");

        String manufacturer = params.get("manufacturer");
        if (manufacturer.isBlank())
            throw new AppRuntimeException("Manufacturer can not be null!");

        String name = params.get("name");
        if (name.isBlank())
            throw new AppRuntimeException("Name can not be null!");

        String material = params.get("material");
        if (material.isBlank())
            throw new AppRuntimeException("Material can not be null!");

        String hasPattern = params.get("hasPattern");
        if (hasPattern.isBlank())
            throw new AppRuntimeException("Has pattern date can not be null!");

        long id;
        long max;
        List<Tableware> tablewares = getAll();
        if (tablewares.isEmpty())
            id = 1;
        else {
            max = tablewares.stream().max(Comparator.comparing(Tableware::getId)).get().getId();
            id = max + 1;
        }
        repository.create(new Tableware(id, Double.valueOf(price), Integer.valueOf(count), manufacturer, name, material, Boolean.valueOf(hasPattern)));
        return id;
    }

    @Override
    public Boolean delete(Map<String, String> params) {
        repository.delete(get(params).getId());
        return true;
    }

    @Override
    public List<Tableware> getAll() {
        Optional<List<Tableware>> tablewareOpt = repository.getAll();
        if (tablewareOpt.isEmpty())
            throw new AppRuntimeException("Tableware not found!");
        return tablewareOpt.get();
    }

    @Override
    public Tableware get(Map<String, String> params) {
        String id = params.get("id");
        if (id.isBlank())
            throw new AppRuntimeException("Id can not be null!");
        return getAll().stream().filter(tableware -> tableware.getId().equals(Long.valueOf(id)))
                .findFirst().orElseThrow(() -> new AppRuntimeException("Tableware not found with this id!"));
    }
}
