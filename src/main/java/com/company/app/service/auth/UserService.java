package com.company.app.service.auth;

import com.company.app.domains.auth.User;
import com.company.app.exception.AppRuntimeException;
import com.company.app.repository.auth.UserRepository;
import com.company.app.service.base.BaseReadService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@AllArgsConstructor
public class UserService implements BaseReadService<User> {

    private UserRepository repository;

    @Override
    public List<User> getAll() {
        Optional<List<User>> userOpt = repository.getAll();
        if (userOpt.isEmpty())
            throw new AppRuntimeException("User not found!");
        return userOpt.get();
    }

    @Override
    public User get(Map<String, String> params) {
        String id = params.get("id");
        if (id.isBlank())
            throw new AppRuntimeException("Id can not be null!");
        Optional<List<User>> userOpt = repository.getAll();
        if (userOpt.isEmpty())
            throw new AppRuntimeException("User not found!");
        return userOpt.get().stream().filter(user -> user.getId().equals(Long.valueOf(id))).
                findFirst().orElseThrow(() -> new AppRuntimeException("User not found with this id!"));
    }

    public User getByUsername(String username) {
        Optional<List<User>> userOpt = repository.getAll();
        if (userOpt.isEmpty())
            throw new AppRuntimeException("User not found!");
        List<User> users = userOpt.get();
        return users.stream().filter(user -> user.getUsername().equals(username))
                .findFirst().orElse(null);
    }

    public void create(User user) {
        repository.create(user);
    }
}
