package com.company.app.service.auth;

import com.company.app.domains.auth.Role;
import com.company.app.domains.auth.UserRole;
import com.company.app.exception.AppRuntimeException;
import com.company.app.repository.auth.RoleRepository;
import com.company.app.repository.auth.UserRoleRepository;
import com.company.app.service.base.BaseReadService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@AllArgsConstructor
public class RoleService implements BaseReadService<Role> {

    private RoleRepository repository;
    private UserRoleRepository userRoleRepository;
    private final UserService userService;

    @Override
    public List<Role> getAll() {
        Optional<List<Role>> roleOpt = repository.getAll();
        if (roleOpt.isEmpty())
            throw new AppRuntimeException("Roles not found!");
        return roleOpt.get();
    }

    @Override
    public Role get(Map<String, String> params) {
        String id = params.get("id");
        if (id.isBlank())
            throw new AppRuntimeException("Id can not be null!");
        return getAll().stream().filter(role -> role.getId().equals(Long.valueOf(id)))
                .findFirst().orElseThrow(() -> new AppRuntimeException("Role not found with this id!"));
    }

    public Role getByUserId(Long userId) {
        UserRole role = userRoleRepository.getByUserId(userId);
        if (!Objects.isNull(role))
            return get(Map.of("id", role.getRoleId().toString()));
        return null;
    }

    public Boolean attachRole(Map<String, String> params) {
        String userId = params.get("userId");
        if (userId.isBlank())
            throw new AppRuntimeException("User id can not be null!");
        if (Objects.isNull(userService.get(Map.of("id", userId))))
            throw new AppRuntimeException("User not found with this id!");

        String roleId = params.get("roleId");
        if (roleId.isBlank())
            throw new AppRuntimeException("Role id can not be null!");
        if (Objects.isNull(get(Map.of("id", roleId))))
            throw new AppRuntimeException("Role not found with this id!");

        userRoleRepository.create(new UserRole(Long.valueOf(userId), Long.valueOf(roleId)));
        return true;
    }
}
