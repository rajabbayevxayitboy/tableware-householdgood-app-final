package com.company.app.service.auth;

import com.company.app.domains.auth.Role;
import com.company.app.domains.auth.User;
import com.company.app.dto.SessionUser;
import com.company.app.exception.AppRuntimeException;
import com.company.app.service.base.BaseAuthService;
import lombok.AllArgsConstructor;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@AllArgsConstructor
public class AuthService implements BaseAuthService<String> {

    private UserService userService;
    private RoleService roleService;

    @Override
    public String login(Map<String, String> params) {
        String username = params.get("username");
        String password = params.get("password");
        if (username.isBlank())
            throw new AppRuntimeException("Username can not be null!");
        if (password.isBlank())
            throw new AppRuntimeException("Password can not be null!");
        User user = userService.getByUsername(username);
        if (Objects.isNull(user))
            throw new AppRuntimeException("User not found with this username!");
        if (!user.getPassword().equals(password))
            throw new AppRuntimeException("Wrong password entered!");
        Role role = roleService.getByUserId(user.getId());
        if (Objects.isNull(role))
            throw new AppRuntimeException("User has no role, please contact the admin!");
        SessionUser instance = SessionUser.getInstance();
        instance.setSessionUser(user);
        instance.setRole(role);
        roleService.getByUserId(user.getId());
        return "You are successfully logged in!";
    }

    @Override
    public String register(Map<String, String> params) {
        String username = params.get("username");
        if (username.isBlank())
            throw new AppRuntimeException("Username can not be null!");

        String password = params.get("password");
        if (password.isBlank())
            throw new AppRuntimeException("Password can not be null!");

        String firstname = params.get("firstname");
        if (firstname.isBlank())
            throw new AppRuntimeException("Firstname not be null!");

        String lastname = params.get("lastname");
        if (lastname.isBlank())
            throw new AppRuntimeException("Lastname can not be null!");

        String age = params.get("age");
        if (age.isBlank())
            throw new AppRuntimeException("Age can not be null!");

        String address = params.get("address");
        if (address.isBlank())
            throw new AppRuntimeException("Address can not be null!");

        User userCheck = userService.getByUsername(username);
        if (!Objects.isNull(userCheck))
            throw new AppRuntimeException("Username already taken!");

        long id;
        long max;
        List<User> users = userService.getAll();
        if (users.isEmpty())
            id = 1;
        else {
            max = users.stream().max(Comparator.comparing(User::getId)).get().getId();
            id = max + 1;
        }
        User user = new User(id, username, password, firstname, lastname, age, address);
        userService.create(user);
        return "You are successfully registered!";
    }
}
