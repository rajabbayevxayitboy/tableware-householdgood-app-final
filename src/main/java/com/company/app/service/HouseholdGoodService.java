package com.company.app.service;

import com.company.app.domains.HouseholdGood;
import com.company.app.exception.AppRuntimeException;
import com.company.app.repository.HouseholdGoodRepository;
import com.company.app.service.base.BaseActionService;
import com.company.app.service.base.BaseReadService;
import lombok.AllArgsConstructor;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class HouseholdGoodService implements BaseActionService<HouseholdGood>, BaseReadService<HouseholdGood> {

    private HouseholdGoodRepository repository;

    @Override
    public Long create(Map<String, String> params) {
        String price = params.get("price");
        if (price.isBlank())
            throw new AppRuntimeException("Price can not be null!");

        String count = params.get("count");
        if (count.isBlank())
            throw new AppRuntimeException("Count can not be null!");

        String manufacturer = params.get("manufacturer");
        if (manufacturer.isBlank())
            throw new AppRuntimeException("Manufacturer can not be null!");

        String name = params.get("name");
        if (name.isBlank())
            throw new AppRuntimeException("Name can not be null!");

        String color = params.get("color");
        if (color.isBlank())
            throw new AppRuntimeException("Color can not be null!");

        String creationDate = params.get("creationDate");
        if (creationDate.isBlank())
            throw new AppRuntimeException("Creation date can not be null!");

        long id;
        long max;
        List<HouseholdGood> householdGoods = getAll();
        if (householdGoods.isEmpty())
            id = 1;
        else {
            max = householdGoods.stream().max(Comparator.comparing(HouseholdGood::getId)).get().getId();
            id = max + 1;
        }
        repository.create(new HouseholdGood(id, Double.valueOf(price), Integer.valueOf(count), manufacturer, name, color, creationDate));
        return id;
    }

    @Override
    public Boolean delete(Map<String, String> params) {
        repository.delete(get(params).getId());
        return true;
    }

    @Override
    public List<HouseholdGood> getAll() {
        return repository.getAll().orElseThrow(()
                -> new AppRuntimeException("Household goods not found!"));
    }

    @Override
    public HouseholdGood get(Map<String, String> params) {
        String id = params.get("id");
        if (id.isBlank())
            throw new AppRuntimeException("Id cannot be null!");
        return repository.getAll().orElseThrow(() -> new AppRuntimeException("Household goods not found!"))
                .stream().filter(householdGood -> householdGood.getId().equals(Long.valueOf(id)))
                .findFirst().orElseThrow(() -> new AppRuntimeException("Household goods not found!"));
    }
}
