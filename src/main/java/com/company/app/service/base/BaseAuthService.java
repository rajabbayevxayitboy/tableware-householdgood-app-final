package com.company.app.service.base;

import java.util.Map;

public interface BaseAuthService<T> {
    T login(Map<String, String> params);

    T register(Map<String, String> params);
}
