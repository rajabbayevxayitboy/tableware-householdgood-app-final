package com.company.app.service.base;

import com.company.app.domains.AbstractEntity;

import java.util.Map;

public interface BaseActionService<T extends AbstractEntity> {
    Long create(Map<String, String> params);

    Boolean delete(Map<String, String> params);
}
