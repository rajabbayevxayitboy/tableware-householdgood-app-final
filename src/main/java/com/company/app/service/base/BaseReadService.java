package com.company.app.service.base;

import com.company.app.domains.AbstractEntity;

import java.util.List;
import java.util.Map;

public interface BaseReadService<T extends AbstractEntity> {
    List<T> getAll();

    T get(Map<String, String> params);
}
