package com.company.app.exception;

import lombok.Getter;

@Getter
public class AppSqlException extends RuntimeException {
    public AppSqlException(String message) {
        super(message);
    }
}
