package com.company.app.repository;

import com.company.app.domains.AbstractEntity;

public interface BaseActionRepository<T extends AbstractEntity> {
    void create(T t);

    void delete(Long id);
}
