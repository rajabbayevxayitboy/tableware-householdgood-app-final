package com.company.app.repository;

import com.company.app.domains.AbstractEntity;

import java.util.List;
import java.util.Optional;

public interface BaseReadRepository<T extends AbstractEntity> {
    Optional<List<T>> getAll();
}
