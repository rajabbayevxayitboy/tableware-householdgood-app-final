package com.company.app.repository;

import com.company.app.domains.Tableware;
import com.company.app.exception.AppRuntimeException;
import com.company.app.exception.AppSqlException;
import com.company.client.helper.CustomFileHandler;
import com.company.client.helper.CustomMapper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class TablewareRepository extends BaseRepository<Tableware> implements BaseActionRepository<Tableware>, BaseReadRepository<Tableware> {

    protected static String path = "src/main/resources/tablewares.csv";

    @Override
    public void create(Tableware tableware) {
        try {
            CustomFileHandler.writeToFile(path, toString(tableware));
        } catch (IOException e) {
            throw new AppSqlException(e.getMessage());
        }
    }

    @Override
    public void delete(Long id) {
        try {
            CustomFileHandler.delete(path, id, "id,price,count,manufacturer,name,material,hasPattern");
        } catch (IOException e) {
            throw new AppSqlException(e.getMessage());
        }
    }

    @Override
    public Optional<List<Tableware>> getAll() {
        try {
            return Optional.of(CustomMapper.toTablewares(CustomFileHandler.readFile(path)));
        } catch (IOException e) {
            throw new AppRuntimeException(e.getMessage());
        }
    }

    public String toString(Tableware tableware) {
        return tableware.getId() + "," +
                tableware.getPrice() + "," +
                tableware.getCount() + "," +
                tableware.getManufacturer() + "," +
                tableware.getName() + "," +
                tableware.getMaterial() + "," +
                tableware.getHasPattern();
    }
}
