package com.company.app.repository;

import com.company.app.domains.HouseholdGood;
import com.company.app.exception.AppSqlException;
import com.company.client.helper.CustomFileHandler;
import com.company.client.helper.CustomMapper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class HouseholdGoodRepository extends BaseRepository<HouseholdGood> implements BaseActionRepository<HouseholdGood>, BaseReadRepository<HouseholdGood> {

    protected static String path = "src/main/resources/household_goods.csv";

    @Override
    public void create(HouseholdGood householdGood) {
        try {
            CustomFileHandler.writeToFile(path, toString(householdGood));
        } catch (IOException e) {
            throw new AppSqlException(e.getMessage());
        }
    }

    @Override
    public void delete(Long id) {
        try {
            CustomFileHandler.delete(path, id, "id,price,count,manufacturer,name,color,creationDate");
        } catch (IOException e) {
            throw new AppSqlException(e.getMessage());
        }
    }

    @Override
    public Optional<List<HouseholdGood>> getAll() {
        try {
            return Optional.of(CustomMapper.toHouseholdGoods(CustomFileHandler.readFile(path)));
        } catch (IOException e) {
            throw new AppSqlException(e.getMessage());
        }
    }

    public String toString(HouseholdGood householdGood) {
        return householdGood.getId() + "," +
                householdGood.getPrice() + "," +
                householdGood.getCount() + "," +
                householdGood.getManufacturer() + "," +
                householdGood.getName() + "," +
                householdGood.getColor() + "," +
                householdGood.getCreationDate();
    }
}
