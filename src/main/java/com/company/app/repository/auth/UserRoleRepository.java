package com.company.app.repository.auth;

import com.company.app.domains.auth.UserRole;
import com.company.app.exception.AppRuntimeException;
import com.company.app.exception.AppSqlException;
import com.company.app.repository.BaseActionRepository;
import com.company.app.repository.BaseRepository;
import com.company.app.repository.BaseReadRepository;
import com.company.client.helper.CustomFileHandler;
import com.company.client.helper.CustomMapper;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class UserRoleRepository extends BaseRepository<UserRole> implements BaseReadRepository<UserRole>, BaseActionRepository<UserRole> {

    protected static String path = "src/main/resources/user_roles.csv";

    @Override
    public void create(UserRole userRole) {
        Optional<List<UserRole>> userRolesOpt = getAll();
        if (userRolesOpt.isEmpty())
            throw new AppRuntimeException("User roles not found!");
        UserRole newUserRole = userRolesOpt.get().stream().filter(userRole1
                -> userRole1.getUserId().equals(userRole.getUserId())).findFirst().orElse(null);
        if (Objects.isNull(newUserRole))
            attach(userRole);
        else
            updateUserRole(userRole);
    }

    private void updateUserRole(UserRole userRole) {
        try {
            CustomFileHandler.delete(path, userRole.getUserId(), "userId,roleId");
            attach(userRole);
        } catch (IOException e) {
            throw new AppSqlException(e.getMessage());
        }
    }

    private void attach(UserRole userRole) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(userRole.getUserId()).append(",");
        stringBuilder.append(userRole.getRoleId());
        try {
            CustomFileHandler.writeToFile(path, stringBuilder.toString());
        } catch (IOException e) {
            throw new AppSqlException(e.getMessage());
        }
    }

    @Override
    public void delete(Long userRole) {
        throw new AppRuntimeException("Method not supported!");
    }

    @Override
    public Optional<List<UserRole>> getAll() {
        try {
            return Optional.of(CustomMapper.toUserRoles(CustomFileHandler.readFile(path)));
        } catch (IOException e) {
            throw new AppRuntimeException(e.getMessage());
        }
    }

    public UserRole getByUserId(Long userId) {
        return getAll().orElseThrow(()
                -> new AppRuntimeException("User-roles not found!")).stream().filter(userRole
                -> userRole.getUserId().equals(userId)).findFirst().orElse(null);
    }
}
