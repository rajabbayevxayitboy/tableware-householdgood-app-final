package com.company.app.repository.auth;

import com.company.app.domains.auth.Role;
import com.company.app.exception.AppRuntimeException;
import com.company.app.repository.BaseRepository;
import com.company.app.repository.BaseReadRepository;
import com.company.client.helper.CustomFileHandler;
import com.company.client.helper.CustomMapper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class RoleRepository extends BaseRepository<Role> implements BaseReadRepository<Role> {

    protected static String path = "src/main/resources/roles.csv";

    @Override
    public Optional<List<Role>> getAll() {
        try {
            return Optional.of(CustomMapper.toRoles(CustomFileHandler.readFile(path)));
        } catch (IOException e) {
            throw new AppRuntimeException(e.getMessage());
        }
    }
}
