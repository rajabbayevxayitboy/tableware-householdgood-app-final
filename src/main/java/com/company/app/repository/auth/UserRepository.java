package com.company.app.repository.auth;

import com.company.app.domains.auth.User;
import com.company.app.exception.AppSqlException;
import com.company.app.repository.BaseRepository;
import com.company.app.repository.BaseReadRepository;
import com.company.client.helper.CustomFileHandler;
import com.company.client.helper.CustomMapper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class UserRepository extends BaseRepository<User> implements BaseReadRepository<User> {

    protected static String path = "src/main/resources/users.csv";

    @Override
    public Optional<List<User>> getAll() {
        try {
            return Optional.of(CustomMapper.toUsers(CustomFileHandler.readFile(path)));
        } catch (IOException e) {
            throw new AppSqlException(e.getMessage());
        }
    }

    public void create(User user) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder.append(user.getId()).append(",");
            stringBuilder.append(user.getUsername()).append(",");
            stringBuilder.append(user.getPassword()).append(",");
            stringBuilder.append(user.getFirstname()).append(",");
            stringBuilder.append(user.getLastname()).append(",");
            stringBuilder.append(user.getAge()).append(",");
            stringBuilder.append(user.getAddress());
            CustomFileHandler.writeToFile(path, stringBuilder.toString());
        } catch (IOException e) {
            throw new AppSqlException(e.getMessage());
        }
    }
}
