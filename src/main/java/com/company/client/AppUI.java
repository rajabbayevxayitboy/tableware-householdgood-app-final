package com.company.client;

import com.company.app.dto.SessionUser;
import com.company.client.constants.Colors;
import com.company.client.helper.ConsoleHelper;
import com.company.client.helper.RequestHandler;

import java.util.HashMap;
import java.util.Map;

public class AppUI {

    public static void run() {
        ConsoleHelper.println("\n\n");
        ConsoleHelper.println("1 -->> Login");
        ConsoleHelper.println("2 -->> Register");
        ConsoleHelper.println("0 -->> Exit");

        String operation = ConsoleHelper.readText("Enter operation number: ");

        switch (operation) {
            case "0" -> System.exit(0);
            case "1" -> login();
            case "2" -> register();
            default -> ConsoleHelper.printError("Invalid operation number: choose available options");
        }
        run();
    }

    private static void register() {
        Map<String, String> params = new HashMap<>();

        ConsoleHelper.println("\n\nRegister page: ");
        ConsoleHelper.print("1.Firstname: ");
        params.put("firstname", ConsoleHelper.readText().trim());

        ConsoleHelper.print("2.Lastname: ");
        params.put("lastname", ConsoleHelper.readText().trim());

        ConsoleHelper.print("3.Username: ");
        params.put("username", ConsoleHelper.readText().trim());

        ConsoleHelper.print("4.Password: ");
        params.put("password", ConsoleHelper.readText().trim());

        ConsoleHelper.print("5.Age: ");
        params.put("age", ConsoleHelper.readText().trim());

        ConsoleHelper.print("6.Address: ");
        params.put("address", ConsoleHelper.readText().trim());

        RequestHandler.sendRequest("/auth/register", params);
    }

    private static void login() {
        Map<String, String> params = new HashMap<>();

        ConsoleHelper.println("\n\nLogin page: ");
        ConsoleHelper.print("1.Username: ");
        params.put("username", ConsoleHelper.readText().trim());

        ConsoleHelper.print("2.Password: ");
        params.put("password", ConsoleHelper.readText().trim());

        RequestHandler.sendRequest("/auth/login", params);
        menu();
    }

    private static void menu() {
        SessionUser sessionUser = SessionUser.getInstance();
        if (sessionUser.getSessionUser() != null) {
            ConsoleHelper.println("\n\nMenu:");
            ConsoleHelper.println("1 -->> Tableware");
            ConsoleHelper.println("2 -->> Household good");
            if (sessionUser.getRole().getCode().equals("ADMIN")) {
                ConsoleHelper.println("3 -->> Show all users");
                ConsoleHelper.println("4 -->> Show all roles");
                ConsoleHelper.println("5 -->> Attach role to user");
            }
            ConsoleHelper.println("6 -->> Profile");
            ConsoleHelper.println("0 -->> Logout");

            String operation = ConsoleHelper.readText("Enter operation number: ");
            switch (operation) {
                case "0" -> logout();
                case "1" -> tablewareUI(sessionUser);
                case "2" -> householdGoodUI(sessionUser);
                case "3" -> showAllUsers();
                case "4" -> showAllRoles();
                case "5" -> attachRole();
                case "6" -> profile();
                default -> ConsoleHelper.printError("Invalid operation number: choose available options");
            }
            menu();
        } else {
            run();
        }
    }

    private static void showAllRoles() {
        RequestHandler.sendRequest("/role/list", null);
    }

    private static void attachRole() {
        Map<String, String> params = new HashMap<>();

        ConsoleHelper.print("\n1.User id: ");
        params.put("userId", ConsoleHelper.readText().trim());

        ConsoleHelper.print("2.Role id: ");
        params.put("roleId", ConsoleHelper.readText().trim());
        RequestHandler.sendRequest("/role/attach-role", params);
    }

    private static void showAllUsers() {
        RequestHandler.sendRequest("/user/list", null);
    }

    private static void householdGoodUI(SessionUser sessionUser) {
        ConsoleHelper.println("\n\n1 -->> Show all");
        ConsoleHelper.println("2 -->> Show by id");
        if (sessionUser.getRole().getCode().equals("ADMIN")) {
            ConsoleHelper.println("3 -->> Create");
            ConsoleHelper.println("4 -->> Delete");
        }
        ConsoleHelper.println("0 -->> Back");
        String operation = ConsoleHelper.readText("Enter operation number: ");
        switch (operation) {
            case "0" -> menu();
            case "1" -> showAllHouseholdGood();
            case "2" -> showHouseHoldGoodById();
            case "3" -> createHouseHoldGood();
            case "4" -> deleteHouseholdGood();
            default -> ConsoleHelper.printError("Invalid operation number: choose available options");
        }
        householdGoodUI(sessionUser);
    }

    private static void tablewareUI(SessionUser sessionUser) {
        ConsoleHelper.println("\n\n1 -->> Show all");
        ConsoleHelper.println("2 -->> Show by id");
        if (sessionUser.getRole().getCode().equals("ADMIN")) {
            ConsoleHelper.println("3 -->> Create");
            ConsoleHelper.println("4 -->> Delete");
        }
        ConsoleHelper.println("0 -->> Back");
        String operation = ConsoleHelper.readText("Enter operation number: ");
        switch (operation) {
            case "0" -> menu();
            case "1" -> showAllTableware();
            case "2" -> showTablewareById();
            case "3" -> createTableware();
            case "4" -> DeleteTableware();
            default -> ConsoleHelper.printError("Invalid operation number: choose available options");
        }
        tablewareUI(sessionUser);
    }

    private static void profile() {
        ConsoleHelper.println(ConsoleHelper.gson.toJson(SessionUser.getInstance()), Colors.YELLOW);
    }

    private static void deleteHouseholdGood() {
        Map<String, String> params = new HashMap<>();
        ConsoleHelper.print("\nEnter id: ");
        params.put("id", ConsoleHelper.readText().trim());
        RequestHandler.sendRequest("/householdGood/delete", params);
    }

    private static void createHouseHoldGood() {
        Map<String, String> params = new HashMap<>();

        ConsoleHelper.print("\n1.Price: ");
        params.put("price", ConsoleHelper.readText().trim());

        ConsoleHelper.print("2.Count: ");
        params.put("count", ConsoleHelper.readText().trim());

        ConsoleHelper.print("3.Manufacturer: ");
        params.put("manufacturer", ConsoleHelper.readText().trim());

        ConsoleHelper.print("4.Name: ");
        params.put("name", ConsoleHelper.readText().trim());

        ConsoleHelper.print("5.Color: ");
        params.put("color", ConsoleHelper.readText().trim());

        ConsoleHelper.print("6.Creation Date: ");
        params.put("creationDate", ConsoleHelper.readText().trim());

        RequestHandler.sendRequest("/householdGood/create", params);
    }

    private static void showHouseHoldGoodById() {
        Map<String, String> params = new HashMap<>();
        ConsoleHelper.print("\nEnter id: ");
        params.put("id", ConsoleHelper.readText().trim());
        RequestHandler.sendRequest("/householdGood/get", params);
    }

    private static void showAllHouseholdGood() {
        RequestHandler.sendRequest("/householdGood/list", null);
    }

    private static void DeleteTableware() {
        Map<String, String> params = new HashMap<>();
        ConsoleHelper.print("\nEnter id: ");
        params.put("id", ConsoleHelper.readText().trim());
        RequestHandler.sendRequest("/tableware/delete", params);
    }

    private static void createTableware() {
        Map<String, String> params = new HashMap<>();

        ConsoleHelper.print("\n1.Price: ");
        params.put("price", ConsoleHelper.readText().trim());

        ConsoleHelper.print("2.Count: ");
        params.put("count", ConsoleHelper.readText().trim());

        ConsoleHelper.print("3.Manufacturer: ");
        params.put("manufacturer", ConsoleHelper.readText().trim());

        ConsoleHelper.print("4.Name: ");
        params.put("name", ConsoleHelper.readText().trim());

        ConsoleHelper.print("5.Material: ");
        params.put("material", ConsoleHelper.readText().trim());

        ConsoleHelper.print("6.Has pattern(true or false): ");
        params.put("hasPattern", ConsoleHelper.readText().trim());

        RequestHandler.sendRequest("/tableware/create", params);
    }

    private static void showTablewareById() {
        Map<String, String> params = new HashMap<>();
        ConsoleHelper.print("\nEnter id: ");
        params.put("id", ConsoleHelper.readText().trim());
        RequestHandler.sendRequest("/tableware/get", params);
    }

    private static void showAllTableware() {
        RequestHandler.sendRequest("/tableware/list", null);
    }

    private static void logout() {
        SessionUser instance = SessionUser.getInstance();
        instance.setSessionUser(null);
        instance.setRole(null);
    }
}
