package com.company.client.constants;

public interface Constants {
    String APP_NAME = "\t\t\t\t\t\t\t\t\t\t\t\tCourse project";
    String VERSION = "1.0";
    String DEV_NAME = "Rajabbayev Xayitboy";
    String DEV_EMAIL = "rajabbayevxayitboy@gmail.com";
}
