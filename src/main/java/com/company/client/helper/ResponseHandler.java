package com.company.client.helper;


import com.company.app.dto.ResponseEntity;
import com.company.app.enums.ResponseStatus;
import com.company.client.constants.Colors;

public class ResponseHandler {

    public static void sendResponse(ResponseEntity responseEntity) {
        ConsoleHelper.println(ConsoleHelper.gson.toJson(responseEntity), Colors.YELLOW);
    }

    public static void sendError(String message) {
        ConsoleHelper.printError(ConsoleHelper.gson.toJson(new ResponseEntity<>(message, ResponseStatus.ERROR)));
    }
}
