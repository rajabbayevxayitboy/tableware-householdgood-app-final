package com.company.client.helper;

import com.company.app.domains.HouseholdGood;
import com.company.app.domains.Tableware;
import com.company.app.domains.auth.Role;
import com.company.app.domains.auth.User;
import com.company.app.domains.auth.UserRole;

import java.util.List;
import java.util.stream.Collectors;

public class CustomMapper {

    public static List<Role> toRoles(List<String> roles) {
        return roles.stream().map(CustomMapper::toRole).collect(Collectors.toList());
    }

    public static Role toRole(String role) {
        String[] strings = role.split(",");
        return new Role(Long.valueOf(strings[0]), strings[1], strings[2]);
    }

    public static List<User> toUsers(List<String> users) {
        return users.stream().map(CustomMapper::toUser).collect(Collectors.toList());
    }

    public static User toUser(String user) {
        String[] strings = user.split(",");
        return new User(Long.valueOf(strings[0]), strings[1], strings[2], strings[3], strings[4], strings[5], strings[6]);
    }

    public static List<Tableware> toTablewares(List<String> tablewares) {
        return tablewares.stream().map(CustomMapper::toTableware).collect(Collectors.toList());
    }

    public static Tableware toTableware(String tableware) {
        String[] strings = tableware.split(",");
        return new Tableware(Long.valueOf(strings[0]), Double.valueOf(strings[1]), Integer.valueOf(strings[2]),
                strings[3], strings[4], strings[5], Boolean.valueOf(strings[6]));
    }

    public static List<HouseholdGood> toHouseholdGoods(List<String> householdGoods) {
        return householdGoods.stream().map(CustomMapper::toHouseholdGood).collect(Collectors.toList());
    }

    public static HouseholdGood toHouseholdGood(String householdGood) {
        String[] strings = householdGood.split(",");
        return new HouseholdGood(Long.valueOf(strings[0]), Double.valueOf(strings[1]), Integer.valueOf(strings[2]),
                strings[3], strings[4], strings[5], strings[6]);
    }

    public static List<UserRole> toUserRoles(List<String> householdGoods) {
        return householdGoods.stream().map(CustomMapper::toUserRole).collect(Collectors.toList());
    }

    public static UserRole toUserRole(String householdGood) {
        String[] strings = householdGood.split(",");
        return new UserRole(Long.valueOf(strings[0]), Long.valueOf(strings[1]));
    }
}
