package com.company.client.helper;

import com.company.app.controller.HouseholdGoodController;
import com.company.app.controller.TablewareController;
import com.company.app.controller.auth.AuthController;
import com.company.app.controller.auth.RoleController;
import com.company.app.controller.auth.UserController;
import com.company.app.dto.ResponseEntity;
import com.company.app.enums.ControllerFactory;
import com.company.app.enums.ResponseStatus;
import com.company.app.exception.AppRuntimeException;

import java.util.Map;

public class RequestHandler {
    private static final TablewareController tablewareController = ControllerFactory.INSTANCE.getTablewareController();
    private static final HouseholdGoodController householdGoodController = ControllerFactory.INSTANCE.getHouseholdGoodController();
    private static final UserController userController = ControllerFactory.INSTANCE.getUserController();
    private static final RoleController roleController = ControllerFactory.INSTANCE.getRoleController();
    private static final AuthController authController = ControllerFactory.INSTANCE.getAuthController();

    public static void sendRequest(String path, Map<String, String> params) {
        try {
            if (path.startsWith("/auth")) {
                ResponseHandler.sendResponse(handleAuthMethod(path, params));
            } else if (path.startsWith("/householdGood")) {
                ResponseHandler.sendResponse(handleHouseholdGoodMethod(path, params));
            } else if (path.startsWith("/tableware")) {
                ResponseHandler.sendResponse(handleTablewareMethod(path, params));
            } else if (path.startsWith("/user")) {
                ResponseHandler.sendResponse(handleUserMethod(path, params));
            } else if (path.startsWith("/role")) {
                ResponseHandler.sendResponse(handleRoleMethod(path, params));
            }
        } catch (Exception e) {
            ResponseHandler.sendError(e.getMessage());
        }
    }

    private static ResponseEntity handleAuthMethod(String path, Map<String, String> params) {
        String methodPath = getMethodPath(path);
        if (methodPath.startsWith("login")) {
            return authController.login(params);
        } else if (methodPath.startsWith("register")) {
            return authController.register(params);
        } else {
            throw new AppRuntimeException("Method not supported!");
        }
    }

    private static ResponseEntity handleHouseholdGoodMethod(String path, Map<String, String> params) {
        String methodPath = getMethodPath(path);
        if (methodPath.startsWith("list")) {
            return householdGoodController.getAll();
        } else if (methodPath.startsWith("get")) {
            return householdGoodController.get(params);
        } else if (methodPath.startsWith("create")) {
            return householdGoodController.create(params);
        } else if (methodPath.startsWith("delete")) {
            return householdGoodController.delete(params);
        }
        return new ResponseEntity<>("Method not supported", ResponseStatus.ERROR);
    }

    private static ResponseEntity handleTablewareMethod(String path, Map<String, String> params) {
        String methodPath = getMethodPath(path);
        if (methodPath.startsWith("list")) {
            return tablewareController.getAll();
        } else if (methodPath.startsWith("get")) {
            return tablewareController.get(params);
        } else if (methodPath.startsWith("create")) {
            return tablewareController.create(params);
        } else if (methodPath.startsWith("delete")) {
            return tablewareController.delete(params);
        }
        return new ResponseEntity<>("Method not supported", ResponseStatus.ERROR);
    }

    private static ResponseEntity handleUserMethod(String path, Map<String, String> params) {
        String methodPath = getMethodPath(path);
        if (methodPath.startsWith("list")) {
            return userController.getAll();
        } else if (methodPath.startsWith("get")) {
            return userController.get(params);
        }
        return new ResponseEntity<>("Method not supported", ResponseStatus.ERROR);
    }

    private static ResponseEntity handleRoleMethod(String path, Map<String, String> params) {
        String methodPath = getMethodPath(path);
        if (methodPath.startsWith("list")) {
            return roleController.getAll();
        } else if (methodPath.startsWith("get")) {
            return roleController.get(params);
        } else if (methodPath.startsWith("attach-role")) {
            return roleController.attachRole(params);
        }
        return new ResponseEntity<>("Method not supported", ResponseStatus.ERROR);
    }

    private static String getMethodPath(String path) {
        String[] split = path.split("/");
        if (split.length > 2)
            return split[2];
        return "";
    }
}
