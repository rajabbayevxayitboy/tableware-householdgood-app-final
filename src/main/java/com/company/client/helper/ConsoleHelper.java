package com.company.client.helper;

import com.company.client.constants.Colors;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Scanner;

import static java.lang.System.out;

public class ConsoleHelper {
    private final static Scanner readText = new Scanner(System.in);

    private final static Scanner readNumerics = new Scanner(System.in);

    public static Gson gson = new GsonBuilder().setDateFormat("dd.MM.YYYY  HH:mm:ss").setPrettyPrinting().create();
    public static Gson gsonWithNulls = (new GsonBuilder()).setPrettyPrinting().serializeNulls().create();

    public static String readText() {
        return readText.nextLine();
    }

    public static Integer readInteger() {
        return readNumerics.nextInt();
    }

    public static Integer readInteger(String data) {
        print(data, Colors.BLUE);
        return readNumerics.nextInt();
    }

    public static double readDouble() {
        return readNumerics.nextDouble();
    }

    public static double readDouble(String data) {
        print(data, Colors.BLUE);
        return readNumerics.nextDouble();
    }


    public static String readText(String data) {
        print(data, Colors.BLUE);
        return readText.nextLine();
    }


    public static String readText(String data, String color) {
        print(data, color);
        return readText.nextLine();
    }

    public static void print(String data) {
        print(data, Colors.BLUE);
    }

    public static void printError(Object data) {
        print(data, Colors.RED);
    }

    public static void print(Object data, String color) {
        out.print(color + data + Colors.RESET);
    }

    public static void println(Object data) {
        println(data, Colors.BLUE);
    }


    public static void println(Object data, String color) {
        out.println(color + data + Colors.RESET);
    }

    public static void println(String data, Object... args) {
        out.printf((data) + "%n", args);
    }
}
